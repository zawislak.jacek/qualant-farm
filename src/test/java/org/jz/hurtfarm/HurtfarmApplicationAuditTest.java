package org.jz.hurtfarm;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.jz.hurtfarm.model.entity.Adres;
import org.jz.hurtfarm.repository.AdresRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class HurtfarmApplicationAuditTest {

    @Autowired
    private AdresRepository adresRepository;

    private Adres adres;

    @BeforeEach
    void setUp() {
        Adres adr =new Adres();
        adr.setMiasto("Lublin");
        adres = adresRepository.save(adr);
    }

    @Test
    void create() {


        assertThat(adres.getCreated())
                .isNotNull();

        assertThat(adres.getModified())
                .isNotNull();

        assertThat(adres.getCreatedBy())
                .isEqualTo("jaczaw");

        assertThat(adres.getModifiedBy())
                .isEqualTo("jaczaw");
    }

    @Test
    void update() {
        LocalDateTime created = adres.getCreated();
        LocalDateTime modified = adres.getModified();

        adresRepository.findById(adres.getId())
                .ifPresent(updatedAdres -> {

                    assertThat(updatedAdres.getMiasto())
                            .isEqualTo("Lublin");

//                    assertThat(updatedAdres.getCreated())
//                            .isEqualToIgnoringNanos(created);
//
//                    assertThat(updatedAdres.getModified())
//                            .isAfter(modified);
                });
    }

}