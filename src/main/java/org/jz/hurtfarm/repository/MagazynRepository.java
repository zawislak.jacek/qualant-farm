package org.jz.hurtfarm.repository;

import org.jz.hurtfarm.model.entity.Magazyn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MagazynRepository extends JpaRepository<Magazyn, Long> {

    Integer getByIlosc(Long produktId);

    @Query("select m from Magazyn m left join m.produkt p where p.id =:id")
    List<Magazyn> getMagazynByProduktId(Long id);
}