package org.jz.hurtfarm.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jz.hurtfarm.model.entity.Sprzedaz;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RejestracjaSprzedazyDto {
    @Valid
    @NotNull
    private KlientDto klientDto;
    private Long klientId;
    @Valid
    @NotNull
    private ProduktDto produktDto;
    private Long produktId;
    private double kwota;
    private Date dataSprzedazy;

    public Sprzedaz convert() {
        Sprzedaz sprzedaz = new Sprzedaz();
        if (klientDto != null) {
            sprzedaz.setKlient(klientDto.convert());
        }
        if (produktDto != null) {
            sprzedaz.setProdukt(produktDto.convert());
        }

        sprzedaz.setKwota(kwota);
        sprzedaz.setDataSprzedazy(dataSprzedazy);
        return sprzedaz;
    }
}
