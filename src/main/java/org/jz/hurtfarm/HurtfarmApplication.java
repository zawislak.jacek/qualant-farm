package org.jz.hurtfarm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HurtfarmApplication {

	public static void main(String[] args) {
		SpringApplication.run(HurtfarmApplication.class, args);
	}

}
