package org.jz.hurtfarm.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jz.hurtfarm.exceptions.ResourceNotFoundException;
import org.jz.hurtfarm.model.dto.ProduktDto;
import org.jz.hurtfarm.model.dto.ResponsePageableDto;
import org.jz.hurtfarm.model.entity.Produkt;
import org.jz.hurtfarm.repository.ProduktRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProduktService {

    private final ProduktRepository produktRepository;

    public Long saveProdukt(Produkt produkt) {
        return produktRepository.save(produkt).getId();
    }

    public List<Produkt> findAllPageable(final ResponsePageableDto responsePageableDto) {
        return produktRepository.findAll(responsePageableDto.toPageRequest()).getContent();
    }

    public List<ProduktDto> findAll() {
        return produktRepository.findAll()
                .stream()
                .map(ProduktDto::convert)
                .collect(Collectors.toList());
    }

    public Produkt getProduktById(final long id) {
        return produktRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Produkt", "id", id));
    }
}
