##Hurtownia Farmaceutyczna API



Spring Hurtownia Farmaceutyczna sample application

#### Getting started

```
git clone https://gitlab.com/zawislak.jacek/qualant-farm
cd qualant-farm
mvn spring-boot:run
```
Authentication:

Login role manager: admin/admin

Login role user: user/user

[OpenApi Documentation swagger-ui](http://localhost:8080/swagger-ui.html) http://localhost:8080/swagger-ui.html

[OpenApi Documentation API Doc](http://localhost:8080/api-docs) http://localhost:8080/api-docs

[Database H2 console](http://localhost:8080/h2-console/) http://localhost:8080/h2-console/




