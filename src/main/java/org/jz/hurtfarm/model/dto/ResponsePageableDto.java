package org.jz.hurtfarm.model.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponsePageableDto {

    private int recordsNumber;
    private int pageNumber;
    private List<String> sortableFields;

    public Pageable toPageRequest() {
        return sortableFields == null || sortableFields.isEmpty()
                ? PageRequest.of(pageNumber, recordsNumber)
                : PageRequest.of(pageNumber, recordsNumber, Sort.by(sortableFields.toArray(new String[]{})));
    }
}