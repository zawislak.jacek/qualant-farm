
INSERT INTO adres (id,miasto,ulica,kod_pocztowy,numer_domu) values(100,'Lublin','Kawaleryjska','20-455','4/15');
INSERT INTO adres (id,miasto,ulica,kod_pocztowy,numer_domu) values (101,'Lublin','Warszawska','20-301','7');
INSERT INTO adres (id,miasto,ulica,kod_pocztowy,numer_domu) values (102,'Lublin','Fabryczna','20-301','6');
INSERT INTO adres (id,miasto,ulica,kod_pocztowy,numer_domu) values (103,'Lublin','Abramowicka','20-442','1');
INSERT INTO adres (id,miasto,ulica,kod_pocztowy,numer_domu) values (104,'Lublin','Nadbystrzycka','20-618','43');
INSERT INTO adres (id,miasto,ulica,kod_pocztowy,numer_domu) values (105,'Lublin','Obywatelska','20-092','8');
INSERT INTO adres (id,miasto,ulica,kod_pocztowy,numer_domu) values (106,'Lublin','Obywatelska','20-092','4/6/8');
INSERT INTO adres (id,miasto,ulica,kod_pocztowy,numer_domu) values (107,'Lublin','Woronieckiego','20-492','9');
INSERT INTO adres (id,miasto,ulica,kod_pocztowy,numer_domu) values (108,'Lublin','Tomasza Zana','20-601','11');
INSERT INTO adres (id,miasto,ulica,kod_pocztowy,numer_domu) values (109,'Lublin','Wolska','20-411','27');
INSERT INTO adres (id,miasto,ulica,kod_pocztowy,numer_domu) values (110,'Lublin','Turystyczna','20-207','11-13');
INSERT INTO adres (id,miasto,ulica,kod_pocztowy,numer_domu) values (111,'Lublin','Świętoduska','20-082','1');
INSERT INTO adres (id,miasto,ulica,kod_pocztowy,numer_domu) values (112,'Lublin','Milenijna','20-884','10');
INSERT INTO adres (id,miasto,ulica,kod_pocztowy,numer_domu) values (113,'Lublin','1 Maja','20-410','5A');
INSERT INTO adres (id,miasto,ulica,kod_pocztowy,numer_domu) values (114,'Lublin','Władysława Orkana','20-504','16A');
INSERT INTO adres (id,miasto,ulica,kod_pocztowy,numer_domu) values (115,'Lublin','Adama Mickiewicza','20-371','4');

INSERT INTO KLIENCI (id,nazwa_firmy,NIP,nr_koncesji,regon,adres_id) VALUES (100,'Apteka "Ziólko"','3419151641','FAA. 3145/A/91','619971144',100);
INSERT INTO KLIENCI (id,nazwa_firmy,NIP,nr_koncesji,regon,adres_id) VALUES (101,'APTEKA NAD RZEKĄ2"','3419151641','LU-WIF.8520.36.20121','619971144',101);
INSERT INTO KLIENCI (id,nazwa_firmy,NIP,nr_koncesji,regon,adres_id) VALUES (102,'APTEKA NAD RZEKĄ"','3419151641','LU-WIF.8520.36.20121','619971144',102);
INSERT INTO KLIENCI (id,nazwa_firmy,NIP,nr_koncesji,regon,adres_id) VALUES (103,'POD KASZTANAMI"','3419151641','LU-WIF.8520.38.20121','619971144',103);
INSERT INTO KLIENCI (id,nazwa_firmy,NIP,nr_koncesji,regon,adres_id) VALUES (104,'Apteka Słoneczna"','3419151641','LU-WIF.8520.11.20131','619971144',104);
INSERT INTO KLIENCI (id,nazwa_firmy,NIP,nr_koncesji,regon,adres_id) VALUES (105,'APTEKA CODZIENNA"','3419151641','WIF/8240/27/041','619971144',105);
INSERT INTO KLIENCI (id,nazwa_firmy,NIP,nr_koncesji,regon,adres_id) VALUES (106,'Apteka Gemini"','3419151641','WIF/9211/303/2000/101','619971144',106);
INSERT INTO KLIENCI (id,nazwa_firmy,NIP,nr_koncesji,regon,adres_id) VALUES (107,'APTEKA CEF@RM 36,6"','3419151641','WIF/9211/289/99/20001','619971144',107);
INSERT INTO KLIENCI (id,nazwa_firmy,NIP,nr_koncesji,regon,adres_id) VALUES (108,'APTEKA NA KOCHANOWSKIEGO"','3419151641','LU-WIF.8520.27.20111','619971144',108);
INSERT INTO KLIENCI (id,nazwa_firmy,NIP,nr_koncesji,regon,adres_id) VALUES (109,'APTEKA ZDROWOTNA"','3419151641','LU-WIF.8520.11.20111','619971144',109);
INSERT INTO KLIENCI (id,nazwa_firmy,NIP,nr_koncesji,regon,adres_id) VALUES (110,'Apteka Piastowska"','3419151641','WIF.8240-31/101','619971144',110);


insert into PRODUKTY (id,symbolPKW,seria,nazwa_handlowa,stawka_VAT) values (100,'PKW/111','5612345','Witamina C',7);

insert into SPRZEDAZ (ID,KWOTA,KLIENT_ID,PRODUKT_ID ) values (100,12.50,100,100);
