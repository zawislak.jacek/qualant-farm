package org.jz.hurtfarm.model.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Table(name = "SPRZEDAZ")
@Entity
@Getter
@Setter
@RequiredArgsConstructor
@ToString(onlyExplicitlyIncluded = true)
public class Sprzedaz implements Serializable {
    private static final long serialVersionUID = 6314614983053380336L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "klient_id", referencedColumnName = "ID")
    private Klient klient;
    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "produkt_id", referencedColumnName = "ID")
    private Produkt produkt;
    private double kwota;
    private Date dataSprzedazy;
    private int ile;
}