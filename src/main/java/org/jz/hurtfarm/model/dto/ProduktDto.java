package org.jz.hurtfarm.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.jz.hurtfarm.model.entity.Produkt;

import java.util.Date;

@Data
@AllArgsConstructor
public class ProduktDto {
    private Long id;
    private String symbolPKW;
    private String seria;
    @ToString.Include
    private String nazwaHandlowa;
    @ToString.Include
    private String nazwaProducenta;
    private String bloz;
    private String ean;
    private Date dataWaznosci;
    private Double cenaNetto;
    private Double cenaBrutto;
    private int stawkaVat;

    public static ProduktDto convert(Produkt produkt) {
        if (produkt == null) {
            return null;
        }
        return new ProduktDto(
                produkt.getId(),
                produkt.getSymbolPKW(),
                produkt.getSeria(),
                produkt.getNazwaHandlowa(),
                produkt.getNazwaProducenta(),
                produkt.getBloz(),
                produkt.getEan(),
                produkt.getDataWaznosci(),
                produkt.getCenaNetto(),
                produkt.getCenaBrutto(),
                produkt.getStawkaVat()
        );
    }

    public Produkt convert() {
        Produkt produkt = new Produkt();
        produkt.setId(id);
        produkt.setSymbolPKW(symbolPKW);
        produkt.setSeria(seria);
        produkt.setNazwaHandlowa(nazwaHandlowa);
        produkt.setNazwaProducenta(nazwaProducenta);
        produkt.setBloz(bloz);
        produkt.setEan(ean);
        produkt.setDataWaznosci(dataWaznosci);
        produkt.setCenaNetto(cenaNetto);
        produkt.setCenaBrutto(cenaBrutto);
        produkt.setStawkaVat(stawkaVat);
        return produkt;
    }
}
