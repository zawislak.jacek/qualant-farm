package org.jz.hurtfarm.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jz.hurtfarm.exceptions.ResourceNotFoundException;
import org.jz.hurtfarm.model.dto.ResponsePageableDto;
import org.jz.hurtfarm.model.dto.SprzedazDto;
import org.jz.hurtfarm.model.entity.Sprzedaz;
import org.jz.hurtfarm.repository.SprzedazRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class SprzedazService {

    private final SprzedazRepository sprzedazRepository;


    public Sprzedaz saveSprzedaz(Sprzedaz sprzedaz) {
        return sprzedazRepository.save(sprzedaz);
    }

    public List<Sprzedaz> findAllPageable(final ResponsePageableDto responsePageableDto) {
        return sprzedazRepository.findAll(responsePageableDto.toPageRequest()).getContent();
    }

    public List<SprzedazDto> findAll() {
        return sprzedazRepository.findAll()
                .stream()
                .map(SprzedazDto::convert)
                .collect(Collectors.toList());
    }

    public Sprzedaz getSprzedazById(final long id) {
        return sprzedazRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Sprzedaz", "id", id));
    }

    public List<SprzedazDto> getSprzedazByKlient(final long klientId) {
        return sprzedazRepository.getSprzedazByKlientId(klientId)
                .stream()
                .map(SprzedazDto::convert)
                .collect(Collectors.toList());

    }

    public List<SprzedazDto> getSprzedazByProdukt(final long produktIt) {
        return sprzedazRepository.getSprzedazByProduktId(produktIt)
                .stream()
                .map(SprzedazDto::convert)
                .collect(Collectors.toList());

    }
}
