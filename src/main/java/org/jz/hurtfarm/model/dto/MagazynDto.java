package org.jz.hurtfarm.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.jz.hurtfarm.model.entity.Magazyn;

@Data
@Builder
@AllArgsConstructor
public class MagazynDto {

    private Long id;
    private Double cena;
    private Integer ilosc;
    @JsonIgnore
    private ProduktDto produktDto;
    private Long productId;


    public static MagazynDto convert(Magazyn magazyn) {
        if (magazyn == null) {
            return null;
        }
        return new MagazynDto(
                magazyn.getId(),
                magazyn.getCena(),
                magazyn.getIlosc(),
                magazyn.getProdukt() != null ? ProduktDto.convert(magazyn.getProdukt()) : null,
                magazyn.getProdukt() != null ? magazyn.getProdukt().getId() : null
        );
    }

    public Magazyn convert() {
        Magazyn magazyn = new Magazyn();
        if (produktDto != null) {
            magazyn.setProdukt(produktDto.convert());
        }
        magazyn.setId(id);
        magazyn.setCena(cena);
        magazyn.setIlosc(ilosc);
        return magazyn;
    }
}
