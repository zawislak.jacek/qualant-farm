package org.jz.hurtfarm.model.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "KLIENCI")
@Entity
@Getter
@Setter
@RequiredArgsConstructor

@ToString(onlyExplicitlyIncluded = true)
public class Klient implements Serializable {
    private static final long serialVersionUID = 5963502222593445119L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nazwaFirmy;

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "ADRES_ID", referencedColumnName = "ID")
    private Adres adres;
    private String nip;
    private String regon;
    private String nrKoncesji;
    private String branza;
    private String status;
}