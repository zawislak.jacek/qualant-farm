package org.jz.hurtfarm.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jz.hurtfarm.model.dto.MagazynDto;
import org.jz.hurtfarm.model.dto.RejestracjaMagazynDto;
import org.jz.hurtfarm.model.dto.ResponsePageableDto;
import org.jz.hurtfarm.model.entity.Magazyn;
import org.jz.hurtfarm.services.MagazynService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/magazyn")
public class MagazynController {

    private final MagazynService magazynService;

    @GetMapping("/{id}")
    public ResponseEntity<Magazyn> getMagazynById(@PathVariable Long id) {
        Magazyn magazyn = magazynService.getMagazynById(id);
        return ResponseEntity.ok(magazyn);
    }

    @PutMapping("/pages")
    public ResponseEntity<List<MagazynDto>> findAllPageable(@RequestBody ResponsePageableDto responsePageableDto) {
        List<MagazynDto> magazynPageDto = magazynService.findAllPageable(responsePageableDto)
                .stream()
                .map(MagazynDto::convert)
                .collect(Collectors.toList());
        return ResponseEntity.ok(magazynPageDto);
    }

    @GetMapping()
    public ResponseEntity<List<MagazynDto>> findAll() {
        List<MagazynDto> magazyn = magazynService.findAll();
        return ResponseEntity.ok(magazyn);
    }


    @PostMapping()
    public ResponseEntity<Long> add(@Valid @RequestBody RejestracjaMagazynDto rejestracjaMagazynDtoDto) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(magazynService.saveMagazyn(rejestracjaMagazynDtoDto.convert()));
    }

    @GetMapping("/produkt/{id}")
    public ResponseEntity<List<MagazynDto>> getMagazynProdukt(@PathVariable Long id) {
        List<MagazynDto> magazyn = magazynService.getMagazynByProdukt(id);
        return ResponseEntity.ok(magazyn);
    }

    @PutMapping("{id}")
    public ResponseEntity updateUser(@PathVariable(value = "id") Long id,
                                                  @Valid @RequestBody MagazynDto newMagaDto) {
        log.info("UPDATE User :  {} ", newMagaDto);
        MagazynDto magazynDto = magazynService.updateStanMag(id, newMagaDto);
        return ResponseEntity.ok(magazynDto);
    }

}
