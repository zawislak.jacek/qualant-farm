package org.jz.hurtfarm.model.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Table(name = "PRODUKTY")
@Entity
@Getter
@Setter
@RequiredArgsConstructor
@ToString(onlyExplicitlyIncluded = true)
public class Produkt implements Serializable {
    private static final long serialVersionUID = -391993942123025872L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ToString.Include
    private Long id;
    private String symbolPKW;
    private String seria;
    @ToString.Include
    private String nazwaHandlowa;
    @ToString.Include
    private String nazwaProducenta;
    private String bloz;
    private String ean;
    private Date dataWaznosci;
    private Double cenaNetto;
    private Double cenaBrutto;
    private int stawkaVat;
    private String skrot;


}