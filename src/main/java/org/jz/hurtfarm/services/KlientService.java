package org.jz.hurtfarm.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jz.hurtfarm.exceptions.ResourceNotFoundException;
import org.jz.hurtfarm.model.dto.KlientDto;
import org.jz.hurtfarm.model.dto.ResponsePageableDto;
import org.jz.hurtfarm.model.entity.Klient;
import org.jz.hurtfarm.repository.KlientRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class KlientService {

    private final KlientRepository klientRepository;

    public Long saveKlient(Klient klient) {
        return klientRepository.save(klient).getId();
    }

    public List<Klient> findAllPageable(final ResponsePageableDto responsePageableDto) {
        return klientRepository.findAll(responsePageableDto.toPageRequest()).getContent();
    }

    public List<KlientDto> findAll() {
        return klientRepository.findAll()
                .stream()
                .map(KlientDto::convert)
                .collect(Collectors.toList());
    }

    public Klient getKlientById(final long id) {
        return klientRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Klient", "id", id));
    }
}
