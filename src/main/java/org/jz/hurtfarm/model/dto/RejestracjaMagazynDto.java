package org.jz.hurtfarm.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jz.hurtfarm.model.entity.Magazyn;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RejestracjaMagazynDto {

    private Long id;
    private Double cena;
    private int ilosc;
    @Valid
    @NotNull
    private ProduktDto produktDto;


    public Magazyn convert() {
        Magazyn magazyn = new Magazyn();
        if (produktDto != null) {
            magazyn.setProdukt(produktDto.convert());
        }
        magazyn.setId(id);
        magazyn.setCena(cena);
        magazyn.setIlosc(ilosc);
        return magazyn;
    }
}
