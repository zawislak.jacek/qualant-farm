package org.jz.hurtfarm.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.naming.ServiceUnavailableException;
import java.time.ZonedDateTime;

@Slf4j
@RestControllerAdvice
public class ApplicationExceptionHandler {


    @ExceptionHandler(value = AdresNotFoundException.class)
    public ResponseEntity<ApiError> handleException(AdresNotFoundException e) {
        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, HttpStatus.NOT_FOUND.value(), e.getLocalizedMessage(), ZonedDateTime.now());
        log.info(String.format("Nie znaleziono danych - %s", e.getLocalizedMessage()));
        return new ResponseEntity<>(apiError, apiError.getHttpStatus());

    }

    @ExceptionHandler(value = ResourceNotFoundException.class)
    public ResponseEntity<ApiError> handleException(ResourceNotFoundException e) {
        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, HttpStatus.NOT_FOUND.value(), e.getLocalizedMessage(), ZonedDateTime.now());
        log.info(String.format("Nie znaleziono zasobu - %s", e.getLocalizedMessage()));
        return new ResponseEntity<>(apiError, apiError.getHttpStatus());
    }


    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiError> handleException(Exception e) {
        ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getLocalizedMessage(), ZonedDateTime.now());
        log.info(String.format("Otrzymano nieznany wyjatek - %s", e.getLocalizedMessage()));
        return new ResponseEntity<>(apiError, apiError.getHttpStatus());
    }

    @ExceptionHandler(PropertyReferenceException.class)
    public ResponseEntity<ApiError> handleException(PropertyReferenceException e) {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.value(), e.getLocalizedMessage(), ZonedDateTime.now());
        log.info(String.format("Nieprawne parametry wejściowe - %s", e.getLocalizedMessage()));
        return new ResponseEntity<>(apiError, apiError.getHttpStatus());
    }


    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ApiError> handleException(MethodArgumentNotValidException e) {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.value(), e.getLocalizedMessage(), ZonedDateTime.now());
        log.info(String.format("Otrzymano nieprawidłowe parametry wejściowe - %s", e.getLocalizedMessage()));
        return new ResponseEntity<>(apiError, apiError.getHttpStatus());
    }


    @ResponseStatus(
            value = HttpStatus.GATEWAY_TIMEOUT,
            reason = "Usługa nie odpowiada, spróbuj ponownie")
    @ExceptionHandler(ServiceUnavailableException.class)
    public ResponseEntity<ApiError> handleException(ServiceUnavailableException e) {
        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, HttpStatus.NOT_FOUND.value(), e.getLocalizedMessage(), ZonedDateTime.now());
        log.info(String.format("Usługa nie odpowiada, spróbuj ponownie - %s", e.getLocalizedMessage()));
        return new ResponseEntity<>(apiError, apiError.getHttpStatus());
    }

}
