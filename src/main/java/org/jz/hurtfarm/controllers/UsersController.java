package org.jz.hurtfarm.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsersController {
    @GetMapping("/users/status/check")
    public String usersStatusCheck() {
        return "Authorized user";
    }

    @GetMapping("/managers/status/check")
    public String managersStatusCheck() {
        return "Authorized manager";
    }

}