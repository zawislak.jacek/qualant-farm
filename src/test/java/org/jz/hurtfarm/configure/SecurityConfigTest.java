package org.jz.hurtfarm.configure;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

class SecurityConfigTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void givenMemUsers_whenGetPingWithValidUser_thenOk() {
        ResponseEntity<String> result
                = makeRestCallToGetPing("user", "user");

        assertThat(result.getStatusCodeValue()).isEqualTo(200);
        assertThat(result.getBody()).isEqualTo("OK");
    }

    @Test
    public void givenExternalUsers_whenGetPingWithValidUser_thenOK() {
        ResponseEntity<String> result
                = makeRestCallToGetPing("admin", "admin");

        assertThat(result.getStatusCodeValue()).isEqualTo(200);
        assertThat(result.getBody()).isEqualTo("OK");
    }

    @Test
    public void givenAuthProviders_whenGetPingWithNoCred_then401() {
        ResponseEntity<String> result = makeRestCallToGetPing();

        assertThat(result.getStatusCodeValue()).isEqualTo(401);
    }

    @Test
    public void givenAuthProviders_whenGetPingWithBadCred_then401() {
        ResponseEntity<String> result
                = makeRestCallToGetPing("user", "bad_password");

        assertThat(result.getStatusCodeValue()).isEqualTo(401);
    }

    private ResponseEntity<String>
    makeRestCallToGetPing(String username, String password) {
        return restTemplate.withBasicAuth(username, password)
                .getForEntity("/users/status/check", String.class, Collections.emptyMap());
    }

    private ResponseEntity<String> makeRestCallToGetPing() {
        return restTemplate
                .getForEntity("/users/status/check", String.class, Collections.emptyMap());
    }

}