package org.jz.hurtfarm.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jz.hurtfarm.model.entity.Klient;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RejestracjaKlientaDto {
    @NotEmpty
    private String nazwaFirmy;
    @Valid
    @NotNull
    private AdresDto adresDto;
    private String nip;
    private String regon;
    private String nrKoncesji;

    public Klient convert() {
        Klient klient = new Klient();
        if (adresDto != null) {
            klient.setAdres(adresDto.convert());
        }
        klient.setNazwaFirmy(nazwaFirmy);
        klient.setNip(nip);
        klient.setRegon(regon);
        klient.setNrKoncesji(nrKoncesji);
        return klient;
    }
}
