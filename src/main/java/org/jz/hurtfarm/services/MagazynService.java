package org.jz.hurtfarm.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jz.hurtfarm.exceptions.ResourceNotFoundException;
import org.jz.hurtfarm.model.dto.MagazynDto;
import org.jz.hurtfarm.model.dto.ResponsePageableDto;
import org.jz.hurtfarm.model.entity.Magazyn;
import org.jz.hurtfarm.repository.MagazynRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class MagazynService {

    private final MagazynRepository magazynRepository;

    public Long saveMagazyn(Magazyn magazyn) {
        return magazynRepository.save(magazyn).getId();
    }

    public List<Magazyn> findAllPageable(final ResponsePageableDto responsePageableDto) {
        return magazynRepository.findAll(responsePageableDto.toPageRequest()).getContent();
    }

    public List<MagazynDto> findAll() {
        return magazynRepository.findAll()
                .stream()
                .map(MagazynDto::convert)
                .collect(Collectors.toList());
    }

    public Magazyn getMagazynById(final long id) {
        return magazynRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Magazyn", "id", id));
    }

    public Magazyn getMagazynByProduktID(final long id) {
        return magazynRepository.getMagazynByProduktId(id).stream().findFirst()
                .orElseThrow(() -> new ResourceNotFoundException("Magazyn", "id", id));
    }

    public List<MagazynDto> getMagazynByProdukt(final long produktId) {
        return magazynRepository.getMagazynByProduktId(produktId)
                .stream()
                .map(MagazynDto::convert)
                .collect(Collectors.toList());

    }

    @Transactional
    public MagazynDto updateStanMag(Long id, MagazynDto nowyStan) {
        return magazynRepository.findById(id)
                .map(magazyn -> {
                    magazyn.setIlosc(nowyStan.getIlosc());
                    magazyn.setCena(nowyStan.getCena());
                    return MagazynDto.convert(magazynRepository.save(magazyn));
                })
                .orElseThrow(() -> new ResourceNotFoundException("UPDATE magazyn", "id", id));
    }
}
