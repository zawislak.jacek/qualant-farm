package org.jz.hurtfarm.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.jz.hurtfarm.model.entity.Adres;

@Data
@AllArgsConstructor
public class AdresDto {

    private Long id;
    private String miasto;
    private String ulica;
    private String kodPocztowy;
    private String numerDomu;

    public static AdresDto convert(Adres adres) {
        if (adres == null) {
            return null;
        }
        return new AdresDto(
                adres.getId(),
                adres.getMiasto(),
                adres.getUlica(),
                adres.getKodPocztowy(),
                adres.getNumerDomu()
        );
    }

    public Adres convert() {
        Adres adres = new Adres();
        adres.setId(id);
        adres.setMiasto(miasto);
        adres.setUlica(ulica);
        adres.setKodPocztowy(kodPocztowy);
        adres.setNumerDomu(numerDomu);
        return adres;
    }
}