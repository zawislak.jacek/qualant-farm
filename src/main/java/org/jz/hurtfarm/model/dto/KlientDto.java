package org.jz.hurtfarm.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.jz.hurtfarm.model.entity.Klient;

@Data
@AllArgsConstructor
public class KlientDto {

    private Long id;
    private String nazwaFirmy;
    @JsonIgnore
    private AdresDto adresDto;
    private Long adresId;
    private String miasto;
    private String ulica;
    private String kodPocztowy;
    private String numerDomu;
    private String nip;
    private String regon;
    private String nrKoncesji;
    private String branza;

    public static KlientDto convert(Klient klient) {
        if (klient == null) {
            return null;
        }
        return new KlientDto(
                klient.getId(),
                klient.getNazwaFirmy(),
                klient.getAdres() != null ? AdresDto.convert(klient.getAdres()) : null,
                klient.getAdres() != null ? klient.getAdres().getId() : null,
                klient.getAdres().getMiasto(),
                klient.getAdres().getUlica(),
                klient.getAdres().getKodPocztowy(),
                klient.getAdres().getNumerDomu(),
                klient.getNip(),
                klient.getRegon(),
                klient.getNrKoncesji(),
                klient.getBranza()
        );
    }

    public Klient convert() {

        Klient klient = new Klient();
        if (adresDto != null) {
            klient.setAdres(adresDto.convert());
        }

        klient.setId(id);
        klient.setNazwaFirmy(nazwaFirmy);
        klient.setNip(nip);
        klient.setRegon(regon);
        klient.setNrKoncesji(nrKoncesji);
        klient.setBranza(branza);
        return klient;
    }
}