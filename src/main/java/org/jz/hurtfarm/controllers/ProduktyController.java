package org.jz.hurtfarm.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jz.hurtfarm.model.dto.ProduktDto;
import org.jz.hurtfarm.model.dto.ResponsePageableDto;
import org.jz.hurtfarm.model.entity.Produkt;
import org.jz.hurtfarm.services.ProduktService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/produkty")
public class ProduktyController {

    private final ProduktService produktService;

    @GetMapping("/{id}")
    public ResponseEntity<Produkt> getAKlientById(@PathVariable Long id) {
        Produkt produkt = produktService.getProduktById(id);
        return ResponseEntity.ok(produkt);
    }

    @PutMapping("/pages")
    public ResponseEntity<List<ProduktDto>> findAllPageable(@RequestBody ResponsePageableDto responsePageableDto) {
       List<ProduktDto> produktyPageDto = produktService.findAllPageable(responsePageableDto)
               .stream()
               .map(ProduktDto::convert)
               .collect(Collectors.toList());
        return ResponseEntity.ok(produktyPageDto);
    }

    @GetMapping()
    public ResponseEntity<List<ProduktDto>> findAll() {
        List<ProduktDto> produkty = produktService.findAll();
        return ResponseEntity.ok(produkty);
    }

    @PostMapping()
    public ResponseEntity<Long> add(@Valid @RequestBody ProduktDto produktDto) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(produktService.saveProdukt(produktDto.convert()));
    }

}
