package org.jz.hurtfarm.repository;

import org.jz.hurtfarm.model.entity.Produkt;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProduktRepository extends JpaRepository<Produkt, Long> {

    Page<Produkt> findAll(Pageable pageable);

}