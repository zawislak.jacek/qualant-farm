package org.jz.hurtfarm.model.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "MAGAZYN")
@Entity
@Getter
@Setter
@RequiredArgsConstructor
@ToString(onlyExplicitlyIncluded = true)
public class Magazyn implements Serializable {
    private static final long serialVersionUID = -6821632862739194327L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Double cena;
    private Integer ilosc;
    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "produkt_id", referencedColumnName = "ID")
    private Produkt produkt;
    private int liczbaPomieszczen;
    private String rodzajMagazynu;
}