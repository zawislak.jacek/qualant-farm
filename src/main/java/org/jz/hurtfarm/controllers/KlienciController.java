package org.jz.hurtfarm.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jz.hurtfarm.model.dto.KlientDto;
import org.jz.hurtfarm.model.dto.RejestracjaKlientaDto;
import org.jz.hurtfarm.model.dto.ResponsePageableDto;
import org.jz.hurtfarm.model.entity.Klient;
import org.jz.hurtfarm.services.KlientService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/kienci")
public class KlienciController {

    private final KlientService klientService;

    @GetMapping("/{id}")
    public ResponseEntity<KlientDto> getAKlientById(@PathVariable Long id) {
        Klient klient = klientService.getKlientById(id);
        return ResponseEntity.ok(KlientDto.convert(klient));
    }

    @PutMapping("/pages")
    public ResponseEntity<List<KlientDto>> findAllPageable(@RequestBody ResponsePageableDto responsePageableDto) {
        List<KlientDto> klientPageDto = klientService.findAllPageable(responsePageableDto)
                .stream()
                .map(KlientDto::convert)
                .collect(Collectors.toList());
        return ResponseEntity.ok(klientPageDto);
    }

    @GetMapping()
    public ResponseEntity<List<KlientDto> > findAll() {
        return ResponseEntity.ok(klientService.findAll());
    }

    @PostMapping()
    public ResponseEntity<Long> add(@Valid @RequestBody RejestracjaKlientaDto rejestracjaKlientaDto) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(klientService.saveKlient(rejestracjaKlientaDto.convert()));
    }
}
