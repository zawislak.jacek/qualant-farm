package org.jz.hurtfarm.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jz.hurtfarm.model.dto.MagazynDto;
import org.jz.hurtfarm.model.dto.RejestracjaSprzedazyDto;
import org.jz.hurtfarm.model.dto.ResponsePageableDto;
import org.jz.hurtfarm.model.dto.SprzedazDto;
import org.jz.hurtfarm.model.entity.Produkt;
import org.jz.hurtfarm.model.entity.Sprzedaz;
import org.jz.hurtfarm.services.MagazynService;
import org.jz.hurtfarm.services.ProduktService;
import org.jz.hurtfarm.services.SprzedazService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/sprzedaze")
public class SprzedazeController {

    private final SprzedazService sprzedazService;
    private final MagazynService magazynService;
    private final ProduktService produktService;

    @GetMapping("/{id}")
    public ResponseEntity<Sprzedaz> getSprzedazById(@PathVariable Long id) {
        Sprzedaz sprzedaz = sprzedazService.getSprzedazById(id);
        return ResponseEntity.ok(sprzedaz);
    }

    @PutMapping("/pages")
    public ResponseEntity<List<SprzedazDto>> findAllPageable(@RequestBody ResponsePageableDto responsePageableDto) {
        List<SprzedazDto> sprzedazePageDto = sprzedazService.findAllPageable(responsePageableDto)
                .stream()
                .map(SprzedazDto::convert)
                .collect(Collectors.toList());
        return ResponseEntity.ok(sprzedazePageDto);
    }

    @GetMapping()
    public ResponseEntity<List<SprzedazDto>> findAll() {
        List<SprzedazDto> sprzedaze = sprzedazService.findAll();
        return ResponseEntity.ok(sprzedaze);
    }

    @PostMapping()
    public ResponseEntity<Sprzedaz> add(@Valid @RequestBody RejestracjaSprzedazyDto rejestracjaSprzedazyDto) {
        Sprzedaz savedSprzedaz = sprzedazService.saveSprzedaz(rejestracjaSprzedazyDto.convert());
        Produkt produkt = savedSprzedaz.getProdukt();
        Integer ileNaStanie = magazynService.getMagazynByProduktID(produkt.getId()).getIlosc();
        magazynService.updateStanMag(savedSprzedaz.getId(),MagazynDto.builder()
                .ilosc((int) (ileNaStanie-rejestracjaSprzedazyDto.getKwota()))
                .cena(produkt.getCenaBrutto())
                .build());
        return ResponseEntity.status(HttpStatus.CREATED).body(savedSprzedaz);
    }

    @GetMapping("/klient/{id}")
    public ResponseEntity<List<SprzedazDto>> getSprzedazKlienta(@PathVariable Long id) {
        List<SprzedazDto> sprzedaze = sprzedazService.getSprzedazByKlient(id);
        return ResponseEntity.ok(sprzedaze);
    }

    @GetMapping("/produkt/{id}")
    public ResponseEntity<List<SprzedazDto>> getSprzedazProduktu(@PathVariable Long id) {
        List<SprzedazDto> sprzedaze = sprzedazService.getSprzedazByProdukt(id);
        return ResponseEntity.ok(sprzedaze);
    }

}
