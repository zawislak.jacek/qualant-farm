package org.jz.hurtfarm.controllers;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jz.hurtfarm.model.dto.AdresDto;
import org.jz.hurtfarm.model.entity.Adres;
import org.jz.hurtfarm.services.AdresService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/adresy")

public class AdresyController {

    private final AdresService adresService;

    @GetMapping("/{id}")
    public ResponseEntity<Adres> getAdresById(@PathVariable Long id) {
        Adres adres = adresService.getAdresById(id);
        return ResponseEntity.ok(adres);
    }

    @GetMapping()
    public ResponseEntity<List<Adres>> getAdresyAll() {
        return ResponseEntity.ok(adresService.getAll());
    }

    @GetMapping("/miasto/{miasto}")
    public ResponseEntity<List<AdresDto>> getAdresByMiasto(@PathVariable String miasto) {
        return ResponseEntity.ok(adresService.getAllAdresByMiasto(miasto));
    }

    @PostMapping
    public ResponseEntity<Long> saveRole(@RequestBody AdresDto adresDto) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(adresService.saveAdres(adresDto).getId());
    }
}
