package org.jz.hurtfarm.repository;

import org.jz.hurtfarm.model.entity.Adres;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdresRepository extends JpaRepository<Adres, Long> {

    List<Adres> findByMiastoAndUlicaAllIgnoreCase(String miasto, String ulica);

    List<Adres> findByMiastoAllIgnoreCase(String miasto);

}