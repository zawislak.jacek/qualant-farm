package org.jz.hurtfarm.repository;

import org.jz.hurtfarm.model.entity.Sprzedaz;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SprzedazRepository extends JpaRepository<Sprzedaz, Long> {

    Page<Sprzedaz> findAll(Pageable pageable);

    @Query("select s from Sprzedaz s left join s.klient k join s.produkt p where k.id =:id")
    List<Sprzedaz> getSprzedazByKlientId(Long id);

    @Query("select s from Sprzedaz s left join s.klient k join s.produkt p where p.id =:id")
    List<Sprzedaz> getSprzedazByProduktId(Long id);


}