package org.jz.hurtfarm.services;

import lombok.RequiredArgsConstructor;
import org.jz.hurtfarm.exceptions.ResourceNotFoundException;
import org.jz.hurtfarm.model.dto.AdresDto;
import org.jz.hurtfarm.model.entity.Adres;
import org.jz.hurtfarm.repository.AdresRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AdresService {

    private final AdresRepository adresRepository;

    public List<Adres> getAdresyMiastoUlica(String misto, String ulica) {
        return adresRepository.findByMiastoAndUlicaAllIgnoreCase(misto, ulica);
    }

    public List<Adres> getAll() {
        return adresRepository.findAll();
    }

    public Adres getAdresById(Long id) {
        return adresRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Adres", "id", id));
    }

    public List<AdresDto> getAllAdres() {
        return adresRepository.findAll()
                .stream()
                .map(AdresDto::convert)
                .collect(Collectors.toList());
    }

    public List<AdresDto> getAllAdresByMiasto(String miasto) {
        return adresRepository.findByMiastoAllIgnoreCase(miasto)
                .stream()
                .map(AdresDto::convert)
                .collect(Collectors.toList());
    }

    public Adres saveAdres(AdresDto adresDto) {
        return adresRepository.save(adresDto.convert());
    }

}
