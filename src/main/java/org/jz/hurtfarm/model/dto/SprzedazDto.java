package org.jz.hurtfarm.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.jz.hurtfarm.model.entity.Klient;
import org.jz.hurtfarm.model.entity.Produkt;
import org.jz.hurtfarm.model.entity.Sprzedaz;

import java.util.Date;

@Data
@AllArgsConstructor
public class SprzedazDto {

    private Long id;
    @JsonIgnore
    private KlientDto klientDto;
    private Long klientId;
    @JsonIgnore
    private ProduktDto produktDto;
    private Long produktId;
    private double kwota;
    private Date dataSprzedazy;

    public static SprzedazDto convert(Sprzedaz sprzedaz) {
        if (sprzedaz == null) {
            return null;
        }
        return new SprzedazDto(
                sprzedaz.getId(),
                sprzedaz.getKlient() != null ? KlientDto.convert(sprzedaz.getKlient()) : null,
                sprzedaz.getKlient() != null ? sprzedaz.getKlient().getId() : null,
                sprzedaz.getProdukt() != null ? ProduktDto.convert(sprzedaz.getProdukt()) : null,
                sprzedaz.getProdukt() != null ? sprzedaz.getProdukt().getId() : null,
                sprzedaz.getKwota(),
                sprzedaz.getDataSprzedazy()
        );
    }

    public Sprzedaz convert() {

        Sprzedaz sprzedaz = new Sprzedaz();
        Klient klient = new Klient();
        klient.setId(klientId);
        Produkt produkt = new Produkt();
        produkt.setId(produktId);
        sprzedaz.setId(id);
        sprzedaz.setKlient(klient);
        sprzedaz.setProdukt(produkt);
        sprzedaz.setKwota(kwota);
        sprzedaz.setDataSprzedazy(dataSprzedazy);
        return sprzedaz;
    }
}
